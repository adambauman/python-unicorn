#!/usr/bin/python
# Test Raspberry Pi -> Arduino I2c communication

import Adafruit_I2C, smbus
import datetime, time
import httplib, urllib, urllib2, cookielib, json, socket
import subprocess, os.path, shutil
import base64

# RSP smbus address
bus = smbus.SMBus(0)

# Arduino slave device address (set on slave, use i2cdetect to verify)
arduinoAddress = 0x04

sendIndex = 0;

arduinoStatus = [0,0,0,0,0,0,0,0]

# Read status data from the micro controller
# 8 bytes: heading LSB | heading MSB | drive batt volt LSB | drive batt volt MSB |
#               control vreg input LSB | control vreg input MSB |
#               wheel sensor move detect | servo rotation good 
#arduinoStatus = bus.read_i2c_block_data(arduinoAddress, 0, 8)
for x in range (0, 7):
    arduinoStatus[x] = bus.read_byte(arduinoAddress)
    time.sleep(2)


print arduinoStatus

# Rock some bit shifting for the 2 byte readings, divide voltages back to floaters
#heading = arduinoStatus[0] + (arduinoStatus[1] << 8)
#driveVoltage = (arduinoStatus[2] + (arduinoStatus[3] << 8)) / float(100)
#controlVoltage = (arduinoStatus[4] + (arduinoStatus[5] << 8)) / float(100)

#print heading

# We only need two points of precision for voltages
#print "%.2f" % driveVoltage
#print "%.2f" % controlVoltage
