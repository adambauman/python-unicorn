#!/usr/bin/python

# Unicorn Manual Keyboard Control Code
# 2014.04.24 Agent Adam Bauman
#
# Raspberry Pi side of the vehicle control system. Use keyboard input to drive the vehicle.

import Adafruit_I2C
import smbus
import time
import urllib
import json

# RSP smbus address
bus = smbus.SMBus(0)

# Arduino slave device address (set on slave, use i2cdetect to verify)
arduinoAddress = 0x04

#DEVICE_ADDRESS = 0X04
#DEVICE_REG_MODE1 = 0X00
#DEVICE_REG_LEDOUT0 = 0X1d

#write single register
#bus.write_byte_data(DEVICE_ADDRES, DEVICE_REG_MODE1, "SUCKA")

#dirForward = [0x66, 0x77]
#dirForwardLeft = [0x66, 0x6c]
#dirForwardRight = [0x66, 0x72]

#dirReverse = [0x72, 0x76]
#dirReverseLeft = [0x72, 0x6c]
#dirReverseRight = [0x72, 0x72]

# Directional values sent through i2c bus
dirForward = 2
dirForwardLeft = 1
dirForwardRight = 3

dirReverse = 5
dirReverseLeft = 4
dirReverseRight = 6 

while(True):
    # grab input
    key = raw_input("W = For, S = Rev, Q = For Left, E = For Right, A = Rev Left, D = Rev Right: ")

    # set command variable for key pressed
    if key == "w":
        selectedCommand = dirForward
    elif key == "q":
        selectedCommand = dirForwardLeft
    elif key == "e":
        selectedCommand = dirForwardRight
    elif key == "s":
        selectedCommand = dirReverse
    elif key == "a":
        selectedCommand = dirReverseLeft
    elif key == "d":
        selectedCommand = dirReverseRight
    else:
        break

    #ship out the control data to the Arduino
    #bus.write_i2c_block_data(address, 0x00, selectedCommand)
    bus.write_byte(address, selectedCommand)

    time.sleep(1)

    #mydata = bus.read_i2c_block_data(address, 4)
    myData = bus.read_byte(address)

    print myData

