# Test status LED blinky circuit

import RPi.GPIO as GPIO
import time

try:
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(7, GPIO.OUT)

    def Blink(numTimes, speed):
        for i in range(0, numTimes):
            print "Iteration " + str(i+1)
            GPIO.output(7, True)
            time.sleep(speed)
            GPIO.output(7, False)
            time.sleep(speed)
        print "Done"
        
    iterations = raw_input("Enter times to blink: ")
    speed = raw_input(" Enter length of each blink: ")

    Blink(int(iterations), float(speed))

except KeyboardInterrupt:
    print "\n Quitting"

except:
    print "Exception error!"

finally:
    GPIO.cleanup()
