#!/usr/bin/python

# Project Unicorn Control Code
# 2014.04.24 Agent Adam Bauman
#
# RSP Side of the vehicle control system. Dummy control code for testing.

#import Adafruit_I2C, smbus
import datetime, time
import httplib, urllib, urllib2, cookielib, json, socket
import subprocess, os.path
import base64, math
import sys


# --------------
# HARDWARE SETUP
# --------------

# RSP smbus address
#bus = smbus.SMBus(0)

# Arduino slave device address (set on slave, use i2cdetect to verify)
#arduinoAddress = 0x04


# --------------
# VARIABLE SETUP
# --------------

heading = 0

# Directional values sent through i2c bus
dirForwardLeft = 1
dirForward = 2
dirForwardRight = 3

dirReverseLeft = 4
dirReverse = 5
dirReverseRight = 6

dirHold = 1973

# Time interval in seconds between control updates
controlTimer = 16

# Control Key required for status update access
key = "SecretKeySharedWithPiCode"

# base URL for the site (change depending on your dev environment)
baseDomain = "badge.local:8080"
baseURL = "http://badge.local:8080/"

# URL to read the control data (include trailing slash!)
controlURL = "control/" + key + "/readvote/"

# URL to write the status data (include trailing slash!)
statusURL = "control/" + key + "/writestatus/"

# login action
loginAction = "processlogin"

# define and setup the cookie container
cookieFile = "mfp.cookies"
cookieJar = cookielib.MozillaCookieJar(cookieFile)

# login credentials
loginUsername = "VehicleLoginUsername"
loginPassword = "VehicleLoginPassword"

# camera capture command
captureCommand = "raspistill -v -n -q 70 -rot 180 -ex auto -t 1500 -w 733 -h 414 -o /home/pi/bast/fscamOutput.jpg"

# enable or disable debug output
debugEnabled = True


# --------------------
# FUNCTION DEFINITIONS
# --------------------

# DbgPrint() - outputs debug information if debugEnabled is True
def DbgPrint(debugText):
    if debugEnabled:
        print(debugText)
        

# WebLogin() - logs into website and saves cookies into webOpener object
def WebLogin():
    # adapted from martinjc.com/2011/06/09/logging-in-to-websites-with-python/
    # send headers and pretend we're a browser
    DbgPrint("\n// LOGGING INTO CONTROL SITE \n")
    try:
        webOpener.addheaders = [('User-agent',
                ('Mozilla/4.0 (compatible; MSIE 6.0; '
                 'Windows NT 5.2; .NET CLR 1.1.4322)'))]

        # encode our login information for POST action
        loginData = urllib.urlencode({
            'loginUsername' : loginUsername,
            'loginPassword' : loginPassword
            })
        
        # build the login URL and open it
        loginURL = baseURL + loginAction
        loginResponse = webOpener.open(loginURL, loginData)

        # save the cookies
        cookieJar.save()
        DbgPrint(" loginURL: " + loginURL)
        DbgPrint(" Login successful")
        #DbgPrint(" Login Response: " + loginResponse.read())
    except:
        DbgPrint(" !!! Login failed !!!")


# CurrentIP() - uses sockets to find the RPI's IP
def GetCurrentIP():
    DbgPrint(" Pulling the RPI's IP")
    try:    
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(("google.com", 80))
        return s.getsockname()[0]
    except:
        network = "0"
        DbgPrint(" !!! IP socket failure !!!")
        return "0.0.0.0"


# CaptureSnapshot() - captures image from camera and uploads
def CaptureSnapshot():
    imagePath = "c:/Python27/projects/bastpi/fscamOutput.jpg"
    
    DbgPrint("\n// CAMERA CAPTURE AND UPLOAD \n")
    try:
        #captureProcess = subprocess.Popen(captureCommand, stdout=subprocess.PIPE, shell=True)
        #captureProcess.wait()
        DbgPrint(" Snapshot saved to disk")
    except:
        camera = "0"
        DbgPrint(" !!! Snapshot capture failed !!!")
        
    # Following code adapted from Damien Lefevre's example
    # www.lfdm.net/thesis/index.php/2007/02/09/41-upload-image-to-url   
    try:
        DbgPrint(" Opening image data")
        try:
            data = open(imagePath, 'rb').read()
        except:
            DbgPrint(" !!! Failed to open image file !!!")

        encodedData = base64.encodestring(data)
        headers = { "Content-type": "application/x-www-form-urlencoded",
                    "Accept": "text/plain",
                    }

        params = urllib.urlencode({ u'fileName': os.path.split(imagePath)[1],
                                    u'data':encodedData })

        DbgPrint(" Opening connection")
        try:
            connection = httplib.HTTPConnection(baseDomain)
            connection.request("POST", "/control/" + key + "/camupload/", params, headers)
            response = connection.getresponse()
            # DbgPrint(" Response received: " + response.read())
        except:
            DbgPrint(" !!! Failed to open uploader connection !!!")
            DbgPrint(response.read())
        
        if response.read() != "Success":
            camera = "0"
            DbgPrint(" !!! Image upload failed !!!")
        else:
            DbgPrint(" Image upload successful")
            
        connection.close()
    except:
        DbgPrint(" !!! UploadImage() failed to execute !!!")


# ----------------
# SINGLE RUN SETUP
# ----------------

# setup web opening object
webOpener = urllib2.build_opener(
    urllib2.HTTPRedirectHandler(),
    urllib2.HTTPHandler(debuglevel=0),
    urllib2.HTTPSHandler(debuglevel=0),
    urllib2.HTTPCookieProcessor(cookieJar)
    )

# login and save the cookies
WebLogin()


# -----------------
# MAIN CONTROL LOOP
# -----------------

while(True):
    # Reset the statusURL for this run
    combinedStatusURL = baseURL + statusURL

    # Reset the status tracking variables
    propBattery = "1"
    controlBattery = "1"
    raspberryPi = "1"
    arduino = "1"
    network = "1"
    controlSync = "1"
    camera = "1"
    steeringServo = "1"
    propMotor = "1"
    suspension = "1"
    tires = "1"
    
    # Grab JSON from the control URL
    DbgPrint("\n// REQUEST CONTROL DATA \n")
    try:
        #DbgPrint(" Control URL: " + baseURL + controlURL)
        response = webOpener.open(baseURL + controlURL)
        controlData = json.loads(response.read())
        DbgPrint(" Data received: " + "{}".format(controlData))
    except:
        controlSync = "0"
        # send login again, hope it works next round
        WebLogin()
        controlData = { 'direction':1973, 'duration':0 }
        DbgPrint(" !!! Control data error !!!")
        DbgPrint(" Tried: " + baseURL + controlURL)
 
    # Send the control data to the Arduino
    # bus.write_i2c_block_data(address, 0x00, selectedCommand)
    DbgPrint("\n// SEND DATA TO MICROCONTROLLER \n")
    try:
        if controlData["direction"] != 1973:
            #bus.write_byte(arduinoAddress, controlData["direction"])
            DbgPrint(" Control code sent")
        else:
            DbgPrint(" Hold command received, skipping transmit")
    except:
        arduino = "0"
        DbgPrint(" !!! Microcontroller communication failure !!!")
    
    # Give the vehicle a chance to move
    time.sleep(3)

    # Capture the camera image and upload
    CaptureSnapshot()

    # Read back the status data
    DbgPrint("\n// RETRIEVE DATA FROM MICROCONTROLLER \n")
    try:
        #arduinoReturnStatus = bus.read_byte(arduinoAddress)
        DbgPrint(" Microcontroller status receieved")
    except:
        arduino = "0"
        DbgPrint(" !!! Microcontroller communication failure !!!")

    # Grab the currentIP
    DbgPrint("\n// REFRESH CURRENT IP ADDRESS \n")
    currentIP = GetCurrentIP()
    DbgPrint(" RPI IP address: " + currentIP)

    # Snag the current heading
    # DUMMY CODE - SIMULATED VALUE
    heading += 10

    # Process error status and set the status keys for each code
    # {commandexecuted}/{propbattery}/{controlbattery}/{propbatterylevel}/{controlbatterylevel}/
    #   {raspberrypi}/{arduino}/{network}/{controlsync}/{camera}/{steeringservo}/{steeringangle}/
    #   {propmotor}/{suspension}/{tires}/{ip}/{heading}/
    statusKeys = (str(controlData["direction"]) + "/" +
                 propBattery + "/" + 
                 controlBattery + "/" + 
                 "0/" + 
                 "0/" + 
                 raspberryPi + "/" + 
                 arduino + "/" + 
                 network + "/" + 
                 controlSync + "/" + 
                 camera + "/" + 
                 steeringServo + "/" + 
                 "0/" + 
                 propMotor + "/" + 
                 suspension + "/" + 
                 tires + "/" + 
                 currentIP + "/" +
                 str(heading) + "/"
                 )

    # Write command data up to the control URL, reset the vote timer
    DbgPrint("\n// TRANSMIT UPDATED STATUS \n")
    combinedStatusURL += statusKeys
    DbgPrint(" Status URL set to: \n" + combinedStatusURL)
    try:
        statusResponse = webOpener.open(combinedStatusURL)
        DbgPrint(" Status transmit response: " + statusResponse.read())
    except:
        DbgPrint(" !!! Status transmit failed !!!")


    DbgPrint("\n// SLEEP UNTIL NEXT UPDATE \n")
    # Loop until the next update is due
    waitLoopCount = controlTimer

    # Time tracking variables
    loopStartTime = datetime.datetime.now()
    loopEndTime = loopStartTime+datetime.timedelta(seconds=controlTimer)
    loopCurrentTime = datetime.datetime.now()
    
    while(loopCurrentTime < loopEndTime):
        loopCurrentTime = datetime.datetime.now()
        DbgPrint(" Waiting..." + "{}".format(waitLoopCount))
        waitLoopCount -= 1
        time.sleep(1)

    DbgPrint("**************** NEXT LOOP *******************\n")
        
