# website login testing
# martinjc.com/2011/06/09/logging-in-to-websites-with-python/

import urllib, urllib2
import cookielib

# base URL for the site
baseURL = 'http://RouteToLoginURL'

# login action
loginAction = 'processlogin'

# define and setup the cookie container
cookieFile = 'mfp.cookies'
cookieJar = cookielib.MozillaCookieJar(cookieFile)

# login credentials
loginUsername = 'LoginTestUsername'
loginPassword = 'LoginTestPassword'

# setup web opening object
webOpener = urllib2.build_opener(
    urllib2.HTTPRedirectHandler(),
    urllib2.HTTPHandler(debuglevel=0),
    urllib2.HTTPSHandler(debuglevel=0),
    urllib2.HTTPCookieProcessor(cookieJar)
    )

def WebLogin():
    # adapted from martinjc.com/2011/06/09/logging-in-to-websites-with-python/
    # send headers and pretend we're a browser
    webOpener.addheaders = [('User-agent',
            ('Mozilla/4.0 (compatible; MSIE 6.0; '
             'Windows NT 5.2; .NET CLR 1.1.4322)'))]

    # encode our login information for POST action
    loginData = urllib.urlencode({
        'loginUsername' : loginUsername,
        'loginPassword' : loginPassword
        })
    
    # build the login URL and open it
    loginURL = baseURL + loginAction
    loginResponse = webOpener.open(loginURL, loginData)

    # save the cookies
    cookieJar.save()


# try it out!
WebLogin()    

response = webOpener.open(baseURL + "control/")
print response.read()
