# Test JSON controller output

import urllib, json, time

url = "http://RouteToJSONTest"

while True:
    response = urllib.urlopen(url)
    data = json.loads(response.read())

    if data["direction"] == 2:
        print "Moving forward!"
    elif data["direction"] == 1:
        print "Moving forward and left!"
    elif data["direction"] == 3:
        print "Moving forward and right!"
    elif data["direction"] == 5:
        print "Moving in reverse!"
    else:
        print "OMG errrrrrorrrr!!!"

    time.sleep(5)

