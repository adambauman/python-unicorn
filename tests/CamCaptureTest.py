# camprocesstest.py
# Test image uploading functions

import httplib, urllib
import base64, os.path

debugEnabled = True

def DbgPrint(debugText):
    if debugEnabled:
        print debugText

def UploadImage(imagePath):
    # Adapted from Damien Lefevre's example
    # www.lfdm.net/thesis/index.php/2007/02/09/41-upload-image-to-url
    
    DbgPrint("| IMAGE UPLOAD |******************************")
    try:
        DbgPrint("Opening image data")
        data = open(imagePath, 'rb').read()

        encodedData = base64.encodestring(data)
        headers = { "Content-type": "application/x-www-form-urlencoded",
                    "Accept": "text/plain",
                    }

        params = urllib.urlencode({ u'fileName': os.path.split(imagePath)[1],
                                    u'data':encodedData })

        DbgPrint("Opening connection")
        connection = httplib.HTTPConnection("BaseDomainURL")
        connection.request("POST", "RouteToUploadController", params, headers)
        response = connection.getresponse()
        
        if response.read() != "Success":
            DbgPrint("Image upload failed")
        else:
            DbgPrint("Image upload successful")
            
        connection.close()

    except:
        DbgPrint("UploadImage() failed to execute")

    DbgPrint("**********************************************")

UploadImage("/home/pi/bast/fscamOutput.jpg")
