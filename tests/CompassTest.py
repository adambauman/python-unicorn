#!/usr/bin/python
import smbus
import time
import math

bus = smbus.SMBus(0)
magAddress = 0x1E

def MagRead(requestedAxis):
    # Adjustments for chip settings
    gain = 1100
    gaussToMicrotesla = 100

    try:
        # Set sample rate, crank to 8 averaged instead of single reading
        bus.write_byte_data(magAddress, 0x00, 0b01110000)
        # Ensure gain is running at 1.3Ga (default)
        #bus.write_byte_data(magAddress, 0x01, 0b00100000)
        # Enable continous measurement
        bus.write_byte_data(magAddress, 0x02, 0b00000000)
    except:
        print "write configuration error"

    # Give the chip some processing time
    time.sleep(0.2)

    if (requestedAxis == "x"):
        HSBaddress = 0x03
        LSBaddress = 0x04
        
    if (requestedAxis == "y"):
        HSBaddress = 0x07
        LSBaddress = 0x08

    # Grab the data
    try:
        magHSB = bus.read_byte_data(magAddress, HSBaddress)
        magLSB = bus.read_byte_data(magAddress, LSBaddress)
    except:
        print "mag read error"
        time.sleep(0.2)
        magHSB = bus.read_byte_data(magAddress, HSBaddress)
        magLSB = bus.read_byte_data(magAddress, LSBaddress)

    # Adjust for signage, then shift them bits
    if (magHSB > 127):
        magHSB -= 256

    #if (magLSB > 127):
    #    magLSB -= 256

    magRaw = magLSB | (magHSB << 8)
    print "magRaw = ", magRaw

    magData = float(magRaw) / gain * gaussToMicrotesla

    return magData

def CompassHeading():
    # Declination adjustment (default 0.22, Richfield is 0.00)
    declinationAngle = 0.00
    
    magX = MagRead("x")
    magY = MagRead("y")

    heading = math.atan2(magY, magX)
    print "atan heading = ", heading
    
    heading += float(declinationAngle)
    print "heading+declination = ", heading

    # Correct for reversed signage
    if(heading < 0):
        heading += 2 * math.pi

    # Adjust for wrap due to declination adjustment
    if(heading > 2 * math.pi):
        heading -= 2 * math.pi

    print "corrected heading = ", heading

    # Convert heading to proper degrees
    headingDegrees = heading * 180 / math.pi

    print "magX = ", magX
    print "magY = ", magY
    print "heading = ", headingDegrees
    print "====================================================================="

    return headingDegrees
    

while True:

    CompassHeading()
    time.sleep(1)
