# Basic timer testing

import datetime
import subprocess
import time

updateHeartbeat = 15

while(True):
    print "Begin loop"

    currentTime = datetime.datetime.now()
    loopTime = currentTime
    updateTime = currentTime + datetime.timedelta(seconds=updateHeartbeat)

    while(loopTime < updateTime):
        loopTime = datetime.datetime.now()
        print loopTime
        time.sleep(1)

    print "End loop"    
    
