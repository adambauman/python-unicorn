from i2clibraries import i2c_hmc5883l

hmc5883l = i2c_hmc5883l.i2c_hmc5883l(0)

setContinuousMode()
hmc5883l.setDeclination(9, 54)

(degrees, minutes) = hmc5883l.getDeclination()
(degrees, minutes) = hmc5883l.getHeading()

heading = hmc5883l.getHeadingString()

print(heading)
