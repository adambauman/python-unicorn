# README #

### Project Unicorn Vehicle-Side Python Software ###
(Summer 2014)

This is the vehicle-side code for Project Unicorn that's responsible for exchanging control and status data with the website and forwarding commands to the chassis control Arduino. 

### Current Status ###

The code in its current format is a bit rough after I pulled out some proprietary code, once I do the same for the web controller I'll clean things up and document it again so it makes sense. In the future I also plan to add real-time control that can take input from devices like Xbox controllers and steering wheels.

### Related Repositories ###
- Arduino Vehicle Chassis Control (https://bitbucket.org/adambauman/arduino_unicorn)