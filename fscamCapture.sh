#!/bin/bash
# Tells fswebcam to capture a snapshot for us

fswebcam -d /dev/video0 -r 768x448 -q --jpeg 60 --no-banner --no-timestamp /home/pi/bast/fscamOutput.jpg

exit 0
