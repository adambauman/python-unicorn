#!/usr/bin/expect
# Script to upload image by FTP to the base site

spawn sftp -q FTPSERVER.WHATEVER.COM
expect "password:"
send "FTPLoginPasswordKeepTheTwoCharactersToTheRight\r"
expect "sftp>"
send "put ImageLocationOnPiKeepSpaceAndTwoCharactersToTheRight \r"
expect "sftp>"
send "bye \r"

exit 0
